ThisBuild / organization := "com.gitlab.vitrox"
ThisBuild / scalaVersion := "2.12.6"

lazy val root = (project in file("."))
  .settings(
    name                := "morpheus-challenge",
    version             := "0.1.1",
    libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5" % Test,
  )
