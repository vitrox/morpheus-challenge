# morpheus-challenge (mc)

The challenge api contains tasks which should be solved in a programmatic way. 
This library written in Scala helps you to access and benchmark them.

## Implementation

### Dependencies

None.

When you need to parse JSON, i recommend the library [Jackson](https://github.com/FasterXML/jackson). 

### Installation

Use JitPack to add this project into your build.

#### sbt

Add the JitPack repository in your build.sbt at the end of resolvers:

```scala
resolvers += "jitpack" at "https://jitpack.io"
```

Add the dependency:

```scala
libraryDependencies += "com.gitlab.vitrox" % "morpheus-challenge" % "0.1.1"	
```

Other JVM languages than Scala were not tested.

See [this](https://jitpack.io/#com.gitlab.vitrox/morpheus-challenge) guide on JitPack for more information or click on the badge.

### Usage

Implement an object with the Challenge trait.

```scala
/** Receives the body of the challenge as a String and sends it unchanged to the solution api
  * The default configuration is used
  * Challenge api: https://cc.the-morpheus.de:443/challenges/0/
  * Solutions api: https://cc.the-morpheus.de:443/solutions/0/
  * This challenge throws a error, because the endpoints don't exist
  */
object ExampleChallenge extends Challenge[String, String] {  
  override def challengeEndpoint = "0"
  override def solutionEndpoint = "0"

  override protected def parseChallenge(input: java.io.InputStream): String = Helper.inputStreamAsString(input)
  override protected def solutionAlgorithm(i: String): String = i
  override protected def sendSolution(o: String, output: java.io.OutputStream): Unit = Helper.writeStringToOutput(o, output)
} 
```

#### Custom target server

Override the server variable with a custom ServerConfig in the Challenge trait.

## Credits

Huge thanks to the german youtuber "[TheMorpheus407](https://www.youtube.com/user/TheMorpheus407)" and his community
for these challenges.