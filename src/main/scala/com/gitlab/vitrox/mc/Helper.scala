/*
 * morpheus-challenge
 * Copyright (C) 2018  Matthias Schwarz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.vitrox.mc

import java.io.{InputStream, OutputStream}

/** Contains helper functions
  */
object Helper {

  def constructToken(token: Any, quotationMarks: Boolean): String =
    s"""{"token":${if (quotationMarks) s""""$token"""" else token}}"""

  @inline def inputStreamAsString(input: InputStream): String = scala.io.Source.fromInputStream(input).mkString

  @inline def writeStringToOutput(string: String, output: OutputStream): Unit =
    output.write(string.getBytes("UTF-8"))

}
