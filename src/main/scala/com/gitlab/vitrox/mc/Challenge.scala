/*
 * morpheus-challenge
 * Copyright (C) 2018  Matthias Schwarz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.vitrox.mc

/** Implement the challenge trait to connect to the challenge api on the target server
  *
  * @tparam I type after parsing the challenge input
  * @tparam O type after running the solution algorithm
  */
trait Challenge[I, O] {
  protected lazy val server: ServerConfig = DefaultServerConfig

  /** Challenge api endpoint
    * Trailing slash '/' is added to the endpoint
    *
    * @return endpoint without trailing slash '/'
    */
  @inline def challengeEndpoint: String

  /** Solution api endpoint
    * Trailing slash '/' is added to the endpoint
    *
    * @return endpoint without trailing slash '/'
    */
  @inline def solutionEndpoint: String

  import java.io.{InputStream, OutputStream}

  /** Contains the logic to parse the input received from the challenge api
    *
    * @param input input stream from the challenge api
    * @return parsed input to process for the solutionAlgorithm
    */
  @inline protected def parseChallenge(input: InputStream): I

  /** Contains main logic for solving the challenge
    *
    * @param i type after parsing challenge input
    * @return object after calculating the solution
    */
  @inline protected def solutionAlgorithm(i: I): O

  /** Contains the logic to parse the solution produced from the solution algorithm and to send it to the output stream
    *
    * @param o produced by solutionAlgorithm
    * @param output the output stream to the solution api
    */
  @inline protected def sendSolution(o: O, output: OutputStream): Unit

  import java.net.{URL, HttpURLConnection}
  private def constructApiUrl(api: String, endpoint: String) =
    new URL("https", server.host, server.port, s"/$api/$endpoint/")

  final lazy val challengeUrl: URL = constructApiUrl(server.challengeApi, challengeEndpoint)
  final lazy val solutionUrl: URL = constructApiUrl(server.solutionApi, solutionEndpoint)

  import javax.net.ssl.HttpsURLConnection
  @inline private def constructApiConnection(url: URL, apply: HttpsURLConnection => Unit) = {
    val connection = url.openConnection().asInstanceOf[HttpURLConnection].asInstanceOf[HttpsURLConnection]
    apply(connection)
    connection.connect()
    connection
  }

  private def getChallengeConnection: HttpsURLConnection = constructApiConnection(challengeUrl, connection => {
    connection.setRequestMethod("GET")
    connection.setDoOutput(true)
  })

  private def getSolutionConnection: HttpsURLConnection = constructApiConnection(solutionUrl, connection => {
    connection.setRequestMethod("POST")
    connection.addRequestProperty("Content-Type", "application/json")
    connection.setDoInput(true)
    connection.setDoOutput(true)
  })

  @inline private def checkSuccess(connection: HttpURLConnection): Unit = {
    val response = contentAsString(connection)
    if (!validSolution(response)) throw new Error(
      s"""Solution was no success
    Response: $response""")
  }

  @inline private def validSolution(response: String) =
    response.startsWith("Success: TMT{") && response.endsWith("}")

  @inline private def contentAsString(connection: HttpURLConnection): String = {
    scala.io.Source.fromInputStream(connection.getInputStream).mkString
  }

  @inline private def checkSolution(solution: O): Unit = {
    val solutionConnection = getSolutionConnection
    sendSolution(solution, solutionConnection.getOutputStream)
    checkSuccess(solutionConnection)
  }

  final def run(): Unit = checkSolution(solutionAlgorithm(parseChallenge(getChallengeConnection.getInputStream)))

  final def benchmark(): Long = {
    val challengeParsed = parseChallenge(getChallengeConnection.getInputStream)
    val (solution, solutionTime) = Internal.measureNanoTime(solutionAlgorithm(challengeParsed))
    checkSolution(solution)
    solutionTime
  }

  final def benchmark(repeats: Int): Product2[Long, Double] = {
    if (repeats < 1) throw new IllegalArgumentException("Invalid repeats")

    var min = Long.MaxValue
    var avg: Double = 0.0

    for (i <- 1 to repeats) {
      val timeTook = benchmark()
      avg += (timeTook - avg)/i
      if (timeTook < min) min = timeTook
    }

    (min, avg)
  }

  final def prettyBenchmark(repeats: Int, printStream: java.io.PrintStream = System.out): Unit = {
    val (benchmarkResult: Product2[Long, Double], timeTook) = Internal.measureMillisTime(benchmark(repeats))
    printStream.println(
      s"""---Benchmark for ${getClass.getSimpleName} with $repeats repeats---
         |---Benchmark took: ${timeTook}ms---
         |---Min time: ${benchmarkResult._1}ns---
         |---Avg time: ${Internal.roundAfterNDigits(benchmarkResult._2, 2)}ns---
       """.stripMargin)
  }

  final def fullBenchmark(): Product3[Long, Long, Long] = {

    val (challengeParsed, receiveChallengeTime: Long) =
      Internal.measureNanoTime(parseChallenge(getChallengeConnection.getInputStream))

    val (solution, solutionTime: Long) = Internal.measureNanoTime(solutionAlgorithm(challengeParsed))

    val (solutionConnection, sendSolutionTime: Long) = Internal.measureNanoTime {
      val solutionConnection = getSolutionConnection
      sendSolution(solution, solutionConnection.getOutputStream)
      solutionConnection
    }
    checkSuccess(solutionConnection)

    (receiveChallengeTime, solutionTime, sendSolutionTime)
  }

  final def debug(): DebugResult = {
    val challengeParsed = parseChallenge(getChallengeConnection.getInputStream)

    val (solution, solutionTime) = Internal.measureNanoTime(solutionAlgorithm(challengeParsed))

    val solutionConnection = getSolutionConnection

    import java.io.ByteArrayOutputStream
    val solutionCache = new ByteArrayOutputStream

    sendSolution(solution, DoubleOutputStream(solutionConnection.getOutputStream, solutionCache))

    val solutionResponse = contentAsString(solutionConnection)
    val cachedSolution = solutionCache.toString("UTF-8")

    val flag = if (validSolution(solutionResponse)) Some(solutionResponse.substring(9)) else None

    DebugResult(cachedSolution, flag, solutionTime)
  }

  final def flag(): Option[String] = debug().flag

}
