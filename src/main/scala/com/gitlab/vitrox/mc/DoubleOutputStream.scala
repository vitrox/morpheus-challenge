/*
 * morpheus-challenge
 * Copyright (C) 2018  Matthias Schwarz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.vitrox.mc

import java.io.OutputStream

/** Splits one output stream to two (may be different) streams
  *
  * @param _1 first output stream
  * @param _2 second output stream
  */
case class DoubleOutputStream(_1: OutputStream, _2: OutputStream)
  extends OutputStream
  with Product2[OutputStream, OutputStream] {

  override def write(b: Int): Unit = {
    _1.write(b)
    _2.write(b)
  }

  override def write(b: Array[Byte]): Unit = {
    _1.write(b)
    _2.write(b)
  }

  override def write(b: Array[Byte], off: Int, len: Int): Unit = {
    _1.write(b, off, len)
    _2.write(b, off, len)
  }

  override def flush(): Unit = {
    _1.flush()
    _2.flush()
  }

  override def close(): Unit = {
    _1.close()
    _2.close()
  }

}
