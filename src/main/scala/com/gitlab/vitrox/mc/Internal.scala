/*
 * morpheus-challenge
 * Copyright (C) 2018  Matthias Schwarz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.vitrox.mc

/** Contains functions for the internal use
  */
private object Internal {
  @inline private def measureTime[R](time: => Long, block: => R): Product2[R, Long] = {
    val start = time
    val result = block
    val end = time
    (result, end-start)
  }

  def measureNanoTime[R](block: => R): Product2[R, Long] = measureTime(System.nanoTime, block)
  def measureMillisTime[R](block: => R): Product2[R, Long] = measureTime(System.currentTimeMillis, block)

  def roundAfterNDigits(n: Double, digits: Int): Double =
    BigDecimal(n).setScale(digits, BigDecimal.RoundingMode.HALF_UP).toDouble

}
