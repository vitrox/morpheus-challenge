/*
 * morpheus-challenge
 * Copyright (C) 2018  Matthias Schwarz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.vitrox.mc

/** This is a valid solution for the first challenge
  * Introduction video (german): https://www.youtube.com/watch?v=q_4OrvVjWhA
  */
object CC1 extends Challenge[String, String] {
  override def challengeEndpoint = "1"
  override def solutionEndpoint = "1"

  override protected def parseChallenge(input: java.io.InputStream): String = Helper.inputStreamAsString(input)

  override protected def solutionAlgorithm(i: String): String = i

  override protected def sendSolution(o: String, output: java.io.OutputStream): Unit =
    Helper.writeStringToOutput(Helper.constructToken(o, quotationMarks = true), output)
}